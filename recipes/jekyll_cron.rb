#
# Cookbook:: ..
# Recipe:: jekyll_cron.rb
#
# Copyright:: 2018, The Authors, All Rights Reserved.
#
include_recipe 'cron'

cron_d 'sync_git_repo' do
  minute '*/5'
  hour '*'
  day '*'
  command  "'cd /var/www/jekyll && git pull origin master'"
  user 'jekyll'
end
